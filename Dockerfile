FROM python:3.9-alpine

# Ensures that the python output gets send to stdout
ENV PYTHONUNBUFFERED=1
# Prevents Python from writing pyc files
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONPATH=/app

ARG ENV
ENV ENV="${ENV}"

ENV HOST="0.0.0.0"

WORKDIR /app
COPY . .

RUN pip install -r requirements.txt

EXPOSE 5000
ENTRYPOINT python manage.py runserver --env $ENV --host $HOST
