from flask import render_template, Blueprint, request, redirect, url_for
from app.modules.elabjournal import *
from app.modules.utils import *
from app.modules.unlockItems import *


profile = Blueprint('profile',
                 __name__,
                 template_folder='/templates/profile',
                 url_prefix='/profile')

@profile.route('/', methods=['GET', 'POST'])
def index():
    return render_template('profile/index.html')


@profile.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('/profile/login.html')
    elif request.method == 'POST':
        return redirect(url_for('main.welcome'))
        