from email.mime import message
from flask import render_template, Blueprint, request, redirect, url_for
from flask import current_app
from app.modules.elabjournal import *
from app.modules.utils import *


main = Blueprint('main',
                 __name__,
                 template_folder='templates/main',
                 url_prefix='/')


@main.route("/")
def home():
    current_app.logger.info('Starting ELabJournal App')
    return render_template("main/index.html", message=None)

@main.route('/', methods=['POST'])
def my_form_post():
    current_app.logger.info('Starting ELabJournal App')
    token = request.form['token']
    try:
        if len(token) == 0:
            return render_template('main/login.html', token=token)
        else:
            status, return_message = login_elabJournal(token)
            if status == True:
                # return redirect(url_for('welcome', username=return_message, token=token))
                current_app.logger.info('Token is already present')
                return render_template("main/welcome.html",
                                    token=token,
                                    userinfo=return_message)
            else:
                current_app.logger.info('Login is not succesful')
                return render_template('main/login.html',
                                    token=token,
                                        message=Message(status="Error", message=return_message))
    except Exception as exc:
        current_app.logger.error(f'Error (my_form_post): {exc} ')
        return render_template('main/login.html',
                            token=token,
                                message=Message(status="Error", message={exc}))


@main.route("/login", methods=['GET'])
def login():
    current_app.logger.info('Calling login screen')
    return render_template("main/login.html", message=None)

@main.route('/login', methods=['POST'])
def my_login_post():
    try:
        current_app.logger.info('Getting POST form loginscreen')
        
        elabJournal_token = request.form['text']
        status, return_message = login_elabJournal(elabJournal_token)
        if status == True:
            # return redirect(url_for('welcome', username="gerrit.seigers@wur.nl", token=token))
            return render_template("main/welcome.html",
                                token=elabJournal_token,
                                userinfo=return_message)
        else:
            current_app.logger.error(f'Error (my_login_post) :{return_message}')
            return render_template("main/login.html", message=Message(status="Warning", message=return_message))
    except Exception as exc:
        current_app.logger.error(f'Fatal Error (my_login_post) :{exc}')
        return render_template("main/login.html", message=Message(status="Error", message={exc}))
        	     


@main.route('/welcome', methods=['GET', 'POST'])
def welcome():
    current_app.logger.info('Calling welcome screen (Welcome)')
    return render_template("main/welcome.html")

@main.route("/logout")
def logout():
    current_app.logger.info('Calling logout screen (logout)')
    return render_template("main/logout.html")