from email import message
from os import stat
from flask import render_template, Blueprint, request, redirect, url_for, session
from flask import current_app
from app.modules.elabjournal import *
from app.modules.utils import *
from app.modules.unlockItems import *

sample = Blueprint('sample',
                   __name__,
                   template_folder='/templates/sample',
                   url_prefix='/sample')


@sample.route("/", methods=['GET'])
def samples():
    return render_template("sample/index.html")


@sample.route('/', methods=['POST'])
def samples_post():

    token = request.form['token']
    sampleID = request.form['code']

    status, data = get_sample_history(token, sampleID)
    if status == True:
        return render_template("sample/index.html", sample_info=data)
    else:
        return render_template("sample/index.html", sample_info=data)


# @sample.route("/history", methods=['GET'])
# def history():
#     current_app.logger.info('Calling sample history')
#     return render_template("sample/history.html")

# @sample.route('/history', methods=['POST'])
# def history_post():

#     try:
#         current_app.logger.info('Getting POST form (history_post)')
#         token = request.form['token']
#         sampleID = request.form['code']

#         #Check if token is a valid Barcode
#         if not is_valid_barcode(sampleID):
#             return render_template("/sample/history.html", message=Message(status='Warning', message='Invalid Elab barcode'))

#         status, data = get_sample_history(token, sampleID)
#         if status == True:
#             return render_template("/sample/history.html", sample_info=data)
#         else:
#             return render_template("/sample/history.html", message=Message(status='Warning', message=f'{data}'))
#     except Exception as exc:
#         current_app.logger.error(f'Error {exc}')
#         return render_template("/sample/history.html", message=Message(status='Error', message={exc}))


@sample.route('/move', methods=['GET'])
def move_get():
    unlock_items = new_unlock_item()
    session['unlock_items'] = unlock_items
    current_app.logger.info('Get on move')
    return render_template('/sample/move.html')


@sample.route('/move', methods=['POST'])
def move_post():
    try:
        current_app.logger.info(f'move_post() started')
        # Getting forms fields back from the post.
        token = request.form['token']
        barcode = request.form['code']
        items = request.form['items']
        action = request.form['action']
        start_sequence = request.form['start_sequence']

        #Get the already scanned items back from the session
        unlock_items = session.get('unlock_items')

        # ............................. B A R   C O D E ........................................    
        if action == 'barcode':
            if not is_valid_barcode(barcode):
                return render_template("/sample/move.html", message=Message(status='Error', message='Invalid barcode scanned'))

            # Get the ELabJournal type of the scanned barcode
            status, elabjournal_barcode_type = get_barcode_type_elabJournal(token, barcode)

            # .................................. T Y P E  I S  S A M P L E ..................................
            if elabjournal_barcode_type['type'] == 'SAMPLE':
                sample_id = elabjournal_barcode_type['id']
                # Check if there is already on Storagelayer scanned.
                if unlock_items['storagelayer'] == None:
                    return redirect(url_for('.move_get', message=f'You have to start with the scan of a equipment of samplecontainer'))

                # Check is this sample is already scanned
                if exits_sample(sample_id=sample_id, unlockItems=unlock_items):
                    return render_template("/sample/move.html", unlock_items=unlock_items,  message=Message(status='Error',
                        message=f'This sample is already scanned.'))

                # Get the sample form ELabJournal
                status, elabjournal_sample_info = get_sample( elabjournal_token=token, sampleId=sample_id)

                # Append the new item to the list and look for space to store in te the container.
                unlock_items['samples'].append(elabjournal_sample_info)
                find_available_space(unlock_items)

                # Throw an error message if there is not enough space to store the samples
                if unlock_items['rows'] > 1 and  unlock_items['max_number_free_sequence'] < len(unlock_items['samples']):
                    return render_template("/sample/move.html", unlock_items=unlock_items,  message=Message(status='Error',
                        message=f'No continues room for this number of samples in this container'))

                session['unlock_items'] = unlock_items                 # Store current unlock_items in a session object.
                return render_template("/sample/move.html", unlock_items=unlock_items)

            # .................................. T Y P E  I S  S T O R A G E L A Y E R ................................
            elif elabjournal_barcode_type['type'] == 'STORAGELAYER':
                storagelayer_id = elabjournal_barcode_type['id']
                status, elabjournal_storagelayer_info = get_storagelayer (elabjournal_token=token, storageLayerId=storagelayer_id)

                # Start with a clean UnlockDictionary
                unlock_items = new_unlock_item()
                session['unlock_items'] =unlock_items                      # Generate a new DICT for the unlock_items.

                unlock_items['rows'] = elabjournal_storagelayer_info['dimension']['rows']['count']
                unlock_items['columns'] = elabjournal_storagelayer_info['dimension']['columns']['count']
                unlock_items['max_number_of_samples'] = get_number_of_storagespace(elabjournal_storagelayer_info)
                
                status, samples = get_storagelayer_samples(elabjournal_token=token, storageLayerId=storagelayer_id)
                add_current_samples(unlock_items, samples)

                unlock_items['storagelayer'] = elabjournal_storagelayer_info
                session['unlock_items'] = unlock_items

                return render_template("/sample/move.html", unlock_items=unlock_items)

            elif elabjournal_barcode_type['type'] == 'SAMPLESERIES':
                return render_template("/sample/move.html", message=Message( status='Error', message=f'Not implemented yet'))
            else:
                return render_template( "/sample/move.html", unlock_items=unlock_items, message=Message( status='Error', message=f'Unrecognized barcode type fron ElabJournal '))

        # ............................. M O V E    S A M P L E S  ........................................    

        elif action == 'moveSamples':
            if unlock_items['rows'] > 1 or unlock_items['rows'] > 1:
                status, message=move_container_samples(elabjournal_token=token, unlockItems=unlock_items, position=int(start_sequence))
            else:
                status, message=move_samples(elabjournal_token=token, unlockItems=unlock_items)
            # Renew unlock_items and refresh cache

            if status == True:
                return redirect(url_for('.move_get', message=f'{message}'))

        # ............................. D E L E T E   S A M P L E ........................................    

        elif action == 'deleteSample':
            sample_id = int(request.form['sample_id'])
            unlock_items = remove_sample(unlock_items, sample_id)
            find_available_space(unlock_items)
            session['unlock_items'] = unlock_items 
                          
            return render_template("/sample/move.html",unlock_items=unlock_items,message=Message(status='Info',message=f'Sample removed'))        
        else:
            return render_template("/sample/move.html",unlock_items=unlock_items,message=Message(status='Error', message='Invalid action returned form postback'))

    except Exception as exc:
        current_app.logger.error(f'Error {exc}')
        return render_template("/sample/move.html",
                               message=Message(status='Error', message={exc}))


@sample.route("/overview", methods=['GET'])
def overview():
    sampleID = request.args.get('sampleId')
    token = request.args.get('token')
    if sampleID != None:
        status, sample_history = get_sample_logs(token, sampleID)
        if status == True:
            return render_template("/sample/overview.html",
                                   sample_info=sample_history)
        else:
            return render_template("/sample/overview.html",
                                   message=Message(status='Error',
                                                   message=sample_history))
    else:
        return render_template("/sample/overview.html")


@sample.route('/overview', methods=['POST'])
def overview_post():
    try:
        token = request.form['token']
        barcode = request.form['code']

        if not is_valid_barcode(barcode):
            return render_template("/sample/overview.html",
                                   message=Message(status='Error',
                                                   message='Invalid barcode'))

        #Get type of barcode
        status, type = get_barcode_type_elabJournal(token, barcode)
        if status == True:
            if type['type'] == 'SAMPLE':
                status, sample_history = get_sample_logs(token, type['id'])
                if status == True:
                    return render_template("/sample/overview.html",
                                           sample_info=sample_history)
                else:
                    return render_template("/sample/overview.html",
                                           message=Message(
                                               status='Error',
                                               message=sample_history))

            elif type['type'] == 'STORAGELAYER':
                status, storagelayer_info = get_storagelayer_info(
                    token, type['id'])
                if status == True:
                    status, storagelayer_history = get_storagelayer_logs(
                        token, type['id'])
                    if status == True:
                        return render_template("/sample/overview.html",
                                               storage_info=storagelayer_info)
                    else:
                        return render_template("/sample/overview.html",
                                               message=Message(
                                                   status='Error',
                                                   message=sample_history))
                else:
                    return render_template("/sample/overview.html",
                                           message=Message(
                                               status='Error',
                                               message=sample_history))

            elif type['type'] == 'SAMPLESERIES':
                return render_template("/sample/overview.html",
                                       message=Message(
                                           status='Info',
                                           message='Not yet implemented'))
            return render_template("/sample/overview.html")
        else:
            return render_template("/sample/overview.html",
                                   message=Message(status='Error',
                                                   message={type}))
    except Exception as exc:
        return render_template("/sample/overview.html",
                               message=Message(status='Error', message={exc}))


# Used for history form Overview screen
@sample.route('/sample_history/<sampleID>/<token>')
def get_sample_history(sampleID, token):
    return redirect(url_for('.overview', sampleId=sampleID, token=token))
