function logout() {
    localStorage.clear("token");
    window.location.href = "/";
};  

function moveSamples() {
   document.getElementById("action").value = "moveSamples";
   document.getElementById("post-form").submit();
};

function clearScans() {
   // clear data and reload
   window.location.pathname = "/sample/track";
};

function sampleHistory(sampleId) {
   let token = localStorage.getItem('token');
   window.location.pathname = "sample/sample_history/" + sampleId + "/" + token
};

//$('.alert').on('closed.bs.alert', function () {
//   $("#code").focus();
//});