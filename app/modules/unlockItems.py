import logging

log = logging.getLogger(__name__)


def new_unlock_item():

    unlock_items = {
        "storagelayer": None,
        "name": None,
        "storageID": None,
        "samples": [],
        "max_number_of_samples": None,
        "max_number_free_sequence": -1,
        "rows": None,
        "columns": None,
        "currentSamples": [],
        "freespaces": []
    }

    for n in range(0, 161):
        unlock_items['currentSamples'].append('Available')

    return unlock_items


def add_current_samples(unlockItems, samples):

    for sample in samples['data']:
        position = sample['position']
        unlockItems['currentSamples'][position] = 'Occupied'

    return unlockItems


def add_storage(unlockItems, name, storage_id, storageLayer_id):
    unlockItems['name'] = name
    unlockItems['storageID'] = storage_id
    unlockItems['storageLayerID'] = storageLayer_id

    return unlockItems


def remove_sample(unlockItems, sample_id):
    found = False

    for n in range(0, len(unlockItems['samples'])):
        if unlockItems['samples'][n]['sampleID'] == sample_id:
            found = True
            break

    if found:
        del unlockItems['samples'][n]

    return unlockItems


def add_sample(unlockItems, sample_id, name):
    unlockItems['samples'].append({'name': name, 'sampleID': sample_id})

    return unlockItems


def exits_sample(unlockItems, sample_id):
    for n in range(0, len(unlockItems['samples'])):
        if unlockItems['samples'][n]['sampleID'] == sample_id:
            return True
    return False

def find_available_space(unlockItems):

    rows = int(unlockItems['rows'])
    columns = int(unlockItems['rows'])
    total_places = rows * columns
    unlockItems['freespaces'] = []
    total_samples = len(unlockItems['samples'])
    end_free_index = 0
    start_free_index = 0
    last_status_found = None

    for i in range(1, total_places +1):
        if unlockItems['currentSamples'][i] != 'Occupied':
            unlockItems['currentSamples'][i] = 'Reserved'

    for i in range(1, total_places +1 ):
        if unlockItems['currentSamples'][i] == 'Occupied':
            if last_status_found == 'Reserved':
                end_free_index = i
                if i != 0:
                    unlockItems['freespaces'].append({'start':start_free_index, 'freespace': end_free_index - start_free_index} )
                    a = 2  # Store in array
            last_status_found = 'Occupied'

        elif unlockItems['currentSamples'][i] == 'Reserved':
            if last_status_found == 'Occupied':        
                start_free_index = i
            last_status_found = 'Reserved'

    if (last_status_found == 'Reserved'):
        unlockItems['freespaces'].append({'start':start_free_index, 'freespace': total_places - end_free_index} )

    counter = 1
    for freespace in unlockItems['freespaces']:
        if len(unlockItems['freespaces']) != counter:
            if freespace['freespace'] >= total_samples:
                start = freespace['start']
                end = freespace['start'] + freespace['freespace'] - total_samples +1
                total_spaces = len(unlockItems['freespaces'])
                for n in range(freespace['start'] ,  freespace['start'] + freespace['freespace'] - total_samples +1):
                    unlockItems['currentSamples'][n] = 'Available'
            counter += 1
        else:
            # This is the last free section.
            for n in range(freespace['start'], total_places - total_samples + 2):
                unlockItems['currentSamples'][n] = 'Available'


# Find the largest container to store samples. 
    for freespace in unlockItems['freespaces']:
        if freespace['freespace'] > unlockItems['max_number_free_sequence']:
            unlockItems['max_number_free_sequence'] = freespace['freespace']

    return            