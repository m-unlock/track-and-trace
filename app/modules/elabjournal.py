import json
import logging
import re
from ast import Raise
from asyncio.log import logger
from datetime import datetime, timezone
from distutils.command.install_lib import install_lib
from signal import raise_signal
from time import timezone
from types import SimpleNamespace
import requests

log = logging.getLogger(__name__)


def login_elabJournal(elabjournal_token):

    url, token = split_elabjournal_token(elabjournal_token)
    header = generate_header(token)

    try:
        log.info(f'Try to login in ELabJournal with token {elabjournal_token}')
        response = requests.get(f"{url}/api/v1/auth/user?body=%7B%7D",
                                headers=header)

        if response.status_code == 200:
            json_data = json.loads(response.text)
            return True, json_data
        elif response.status_code == 401:
            return False, "Unauthorize"
        elif response.status_code == 500:
            return False, "Server error 500"
        else:
            return False, "Something went wrong"

    except Exception as exc:
        log.error(f'Fatal error (login_elabJournal): {exc}')
        return False, f'Fatal error: {exc}'


def get_barcode_type_elabJournal(elabjournal_token, code):
    url, token = split_elabjournal_token(elabjournal_token)
    header = generate_header(token)
    try:
        log.info(f'Try to get type of Barcode {code}')
        response = requests.get(f"{url}/api/v1/barcode/{code}", headers=header)
        if response.status_code == 200:
            type = json.loads(response.text)
            return True, type
        else:
            log.error(f'get_barcode_type_elabJournal(): status_code: {response.status_code} text: {response.text}')
            Raise(response.text)

    except Exception as exc:
        log.error(f'Fatal error (login_elabJournal): {exc}')
        Raise(exc)


def get_sample_logs(elabjournal_token, sampleId):

    url, token = split_elabjournal_token(elabjournal_token)
    header = generate_header(token)

    try:
        response = requests.get(f"{url}/api/v1/samples/{sampleId}/logs",
                                headers=header)
        if response.status_code == 200:
            type = json.loads(response.text)
            # Modify some text to make it more readable in the views
            for item in type['data']:
                oldValue = item['oldValue']
                newValue = item['newValue']
                item["oldStorage"] = oldValue[0:oldValue.find('(') - 1]
                item["newStorage"] = newValue[0:newValue.find('(') - 1]
                # Take care of the date. It's a string and we want a date object
                item["modified_date"] = datetime.strptime(item['changed'],'%Y-%m-%dT%H:%M:%SZ')
            
            type['data'] = sorted(type['data'], key=lambda d: d['modified_date'], reverse=True)
            return True, type
        else:
            return False, response.content

    except Exception as exc:
        log.error(f'Fatal error (get_sample_history): {exc}')
        return False, f'Fatal error: {exc}'


def get_storagelayer(elabjournal_token, storageLayerId):
    url, token = split_elabjournal_token(elabjournal_token)
    header = generate_header(token)
    try:
        log.info(f'get_storagelayer called for sampleId: {storageLayerId}')
        response = requests.get(f"{url}/api/v1/storageLayers/{storageLayerId}",
                                headers=header)
        if response.status_code == 200:
            storageLayers = json.loads(response.text)
            return True, storageLayers
        else:
            log.error(f'get_storagelayer() status_code: {response.status_code} status_text: {response.text}')
            Raise(f'get_storagelayer() status_code: {response.status_code} status_text: {response.text}')    

    except Exception as exc:
        log.error(f'{exc}')
        Raise(exc)

def get_sample(elabjournal_token, sampleId):
    url, token = split_elabjournal_token(elabjournal_token)
    header = generate_header(token)
    try:
        log.info(f'get_sample() called for sampleId: {sampleId}')
        response = requests.get(f"{url}/api/v1/samples/{sampleId}",
                                headers=header)
        if response.status_code == 200:
            sample = json.loads(response.text)
            return True, sample

        else:
            log.error(f'get_sample() status_code: {response.status_code} status_text: {response.text}')
            Raise(f'get_sample() status_code: {response.status_code} status_text: {response.text}')    

    except Exception as exc:
        log.error(f'{exc}')
        Raise(exc)


def get_storagelayer_samples(elabjournal_token, storageLayerId):
    url, token = split_elabjournal_token(elabjournal_token)
    header = generate_header(token)
    try:
        log.info(f'get_storagelayer_samples() called for storageLayerId: {storageLayerId}')
        response = requests.get(f"{url}/api/v1/storageLayers/{storageLayerId}/samples", headers=header)
        if response.status_code == 200:
            sample = json.loads(response.text)
            return True, sample
        else:
            log.error(f'get_storagelayer_samples() status_code: {response.status_code} status_text: {response.text}')
            Raise(f'get_storagelayer_samples() status_code: {response.status_code} status_text: {response.text}')    

    except Exception as exc:
        log.error(f'{exc}')
        Raise(exc)


def get_storagelayer_logs(elabjournal_token, storageLayerId):
    url, token = split_elabjournal_token(elabjournal_token)
    header = generate_header(token)

    try:
        response = requests.get(
            f"{url}/api/v1/storageLayers/{storageLayerId}/samples",
            headers=header)
        if response.status_code == 200:
            type = json.loads(response.text)
            return True, type
        else:
            return False, response.content

    except Exception as exc:
        log.error(f'Fatal error (get_sample_history): {exc}')
        return False, f'Fatal error: {exc}'


def get_sublayers_info(sublayer, elabjournal_token, storageLayerId):

    url, token = split_elabjournal_token(elabjournal_token)
    header = generate_header(token)

    try:
        response = requests.get(
            f"{url}/api/v1/storageLayers/{storageLayerId}/childLayers",
            headers=header)
        if response.status_code == 200:
            type = json.loads(response.text)
            # Find all samples that are in this storageLayer
            for item in type['data']:
                status, samples = get_storagelayer_logs(
                    elabjournal_token=elabjournal_token,
                    storageLayerId=item['storageLayerID'])
                if status == True:
                    item['samples'] = samples
                    item['sublayers'] = []
                    get_sublayers_info(item['sublayers'], elabjournal_token,
                                       item['storageLayerID'])
                    sublayer.append(item)

            return True, type
        else:
            return False, response.content

    except Exception as exc:
        log.error(f'Fatal error (get_sublayers_info): {exc}')
        return False, f'Fatal error: {exc}'


def get_storagelayer_info(elabjournal_token, storageLayerId):

    url, token = split_elabjournal_token(elabjournal_token)
    header = generate_header(token)

    try:
        response = requests.get(f"{url}/api/v1/storageLayers/{storageLayerId}",
                                headers=header)
        if response.status_code == 200:
            type = json.loads(response.text)

            # Get the samples for this StorageLayer
            status, samples = get_storagelayer_logs(
                elabjournal_token=elabjournal_token,
                storageLayerId=f'{storageLayerId}')
            if status == True:
                type['samples'] = samples
            else:
                Raise

            type['sublayers'] = []  #Declare emprty arrar for the sublayers
            get_sublayers_info(type['sublayers'],
                               elabjournal_token=elabjournal_token,
                               storageLayerId=type['storageLayerID'])
            a = json.dumps(type)
            return True, type
        else:
            return False, response.content

    except Exception as exc:
        log.error(f'Fatal error (get_storagelayer_info): {exc}')
        return False, f'Fatal error: {exc}'


def get_type_elabJournal(elabjournal_token, code):

    url, token = split_elabjournal_token(elabjournal_token)
    header = generate_header(token)

    try:
        log.info(f'Try to get type of Barcode {code}')
        response = requests.get(f"{url}/api/v1/barcode/{code}", headers=header)
        if response.status_code == 200:
            type = json.loads(response.text,
                              object_hook=lambda d: SimpleNamespace(**d))
            if type.type == 'SAMPLE':
                # type.id bevat de code van het sample
                response = requests.get(f"{url}/api/v1/samples/{type.id}",
                                        headers=header)
                if response.status_code == 200:
                    sample = json.loads(
                        response.text,
                        object_hook=lambda d: SimpleNamespace(**d))
                    sample.unlock_type = 'sample'
                    return True, sample
            elif type.type == 'STORAGELAYER':
                response = requests.get(
                    f"{url}/api/v1/storageLayers/{type.id}", headers=header)
                if response.status_code == 200:
                    storageLayers = json.loads(
                        response.text,
                        object_hook=lambda d: SimpleNamespace(**d))
                    storageLayers.unlock_type = 'storageLayers'
                    return True, storageLayers
            elif type.type == 'SAMPLESERIES':
                response = requests.get(f"{url}/api/v1/sampleSeries/{type.id}",
                                        headers=header)
                if response.status_code == 200:
                    sampleSeries = json.loads(
                        response.text,
                        object_hook=lambda d: SimpleNamespace(**d))
                    sampleSeries.unlock_type = 'sampleSeries'
                    return True, sampleSeries
            return True, "hdhd"
        else:
            return False, f'Server response ELabJournal {response.status_code }'

    except Exception as exc:
        log.error(f'Fatal error (get_type_elabJournal): {exc}')
        return False, f'Fatal Error: {exc}'


def move_container_samples(elabjournal_token, unlockItems, position):
    try:
        url, token = split_elabjournal_token(elabjournal_token)
        header = generate_header(token)

        storageLayerID = unlockItems['storagelayer']['storageLayerID']
        log.info(f'move_container_samples() called for storageLayerID: {storageLayerID}')

        counter = 0
        for sample in unlockItems['samples']:
            sampleID = sample['sampleID']
            log.info(f'move_container_samples() try to move sampleID {sampleID} to storageLayerID {storageLayerID} to position {position + counter}')


            payload = '{' + '"position":' + str(position+counter) + '}'
            response = requests.post(f"{url}/api/v1/samples/{sampleID}/moveToLayer/{storageLayerID}", headers=header, data=payload)

            if response.status_code != 204:
                log.error(f'move_container_samples() status_code: {response.status_code} response text: {response.text}')
                Raise (f'move_container_samples() status_code: {response.status_code} response text: {response.text}')

            counter = counter + 1
        return True, f"Samples are succesfully moved to {unlockItems['storagelayer']['name']} starting at position {position}"
    except Exception as exc:
        log.error(f'{exc}')
        Raise (exc)


def move_samples(elabjournal_token, unlockItems):
    try:
        url, token = split_elabjournal_token(elabjournal_token)
        header = generate_header(token)
        storageLayerID = unlockItems['storagelayer']['storageLayerID']
        log.info(f'move_samples() called for storageLayerID: {storageLayerID}')

        payload = '{}'

        for sample in unlockItems['samples']:
            sampleID = sample['sampleID']
            log.info(f'move_samples() try to move sampleID {sampleID} to storageLayerID {storageLayerID}')
            response = requests.post(f"{url}/api/v1/samples/{sampleID}/moveToLayer/{storageLayerID}",headers=header,data=payload)

            if response.status_code != 204:
                log.error(f'move_samples() status_code: {response.status_code} response text: {response.text}')
                Raise (f'move_samples() status_code: {response.status_code} response text: {response.text}')

        return True, f"Samples are succesfully moved to storagelayer {unlockItems['storagelayer']['name']}"

    except Exception as exc:
        log.error(f'{exc}')
        Raise (exc)


def get_sample_history(elabjournal_token, code):

    try:
        url, token = split_elabjournal_token(elabjournal_token)
        header = generate_header(token)

        # Translate a first the barcode to an SampleId
        response = requests.get(f"{url}/api/v1/barcode/{code}", headers=header)
        if response.status_code == 200:
            type = json.loads(response.text,
                              object_hook=lambda d: SimpleNamespace(**d))
            if type.type == 'SAMPLE':
                response = requests.get(f"{url}/api/v1/samples/{type.id}/logs",
                                        headers=header)
                if response.status_code == 200:
                    return True, json.loads(
                        response.text,
                        object_hook=lambda d: SimpleNamespace(**d))
                else:
                    return True, response.content
            else:
                return False, "Not a valid storage"

        else:
            return False, "Error get code by Barcode"
    except Exception as inst:
        log.error(f'{inst}')
        return False, f'{inst}'


def get_inventory_info(elabjournal_token, code):

    try:
        url, token = split_elabjournal_token(elabjournal_token)
        header = generate_header(token)

        # Translate a first the barcode to an SampleId
        response = requests.get(f"{url}/api/v1/barcode/{code}", headers=header)
        if response.status_code == 200:
            type = json.loads(response.text,
                              object_hook=lambda d: SimpleNamespace(**d))
            if type.type == 'STORAGELAYER':
                # We have to get the storageID for this StorageLayer
                response = requests.get(
                    f'{url}/api/v1/storageLayers/{type.id}', headers=header)
                if response.status_code == 200:
                    #get the storageId out of the response.
                    storage_layer_type = json.loads(
                        response.text,
                        object_hook=lambda d: SimpleNamespace(**d))
                    storage_layer_name = storage_layer_type.name
                    response = requests.get(
                        f"{url}/api/v1/storage/{storage_layer_type.storageID}/samples",
                        headers=header)
                    if response.status_code == 200:
                        return True, json.loads(
                            response.text,
                            object_hook=lambda d: SimpleNamespace(
                                **d)), storage_layer_name
                else:
                    return True, response.content
            else:
                return False, "Not a valid storagelayer"

        else:
            return False, "Error get code by Barcode"
    except Exception as exc:
        log.error(f'{exc}')
        return False, f'{exc}'


def generate_header(token):
    return {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": f"{token}"
    }


def split_elabjournal_token(elabjournal_token):
    try:
        fields = elabjournal_token.split(';')
        if len(fields) != 2:
            raise Exception(f'Invalid ELabJournal token: {elabjournal_token}')
        return f'https://{fields[0]}', fields[1]
    except Exception as exc:
        raise Exception
