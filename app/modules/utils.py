import logging
import base64
import json
import re

log = logging.getLogger(__name__)


class Message:

    def __init__(self, status, message):
        self.status = status
        self.message = message


def toBase64(data):
    j = json.dumps(data)
    b = j.encode("UTF-8")
    e = base64.b64encode(b)
    return e.decode("UTF-8")


def fromBase64(data):
    b1 = data.encode("UTF-8")
    d = base64.b64decode(b1)
    message_layer = d.decode("UTF-8")
    return json.loads(message_layer)


def is_valid_barcode(input):
    return bool(re.search(r'\d', input))


def get_number_of_storagespace(elabjournal_storagelayer_info):
    rows = elabjournal_storagelayer_info['dimension']['rows']['count']
    columns = elabjournal_storagelayer_info['dimension']['columns']['count']

    if rows * columns > 1:
        return rows * columns
    else:
        return None
