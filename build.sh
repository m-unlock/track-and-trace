#!/bin/bash
#============================================================================
#title          :Unlock Track and Trace Project
#description    :Track and trace system for eLabjournal
#author         :Gerrit Seigers, Jasper Koehorst
#date           :2021
#version        :0.0.1
#============================================================================
docker login registry.gitlab.com

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

#============================================================================
# Build the docker file
#============================================================================

# docker pull docker-registry.wur.nl/unlock/docker:cwl

docker build -t registry.gitlab.com/m-unlock/track-and-trace .

docker push registry.gitlab.com/m-unlock/track-and-trace